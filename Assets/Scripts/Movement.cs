﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour
{
    private Animator animator;
    private AnimatorStateInfo currentState;
    private float inputX;
    private bool isGrounded;

    private Rigidbody2D rigid;
    private const int MAX_JUMPS = 2;
    private int numOfJumps;


    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody2D>();

        ResetJumps();
    }

    // Update is called once per frame
    void Update()
    {
        AnimatorUpdate();

        if(Input.GetButtonDown("Jump"))
        {
            Jump();
        }

       
    }

    private void AnimatorUpdate()
    {
        currentState = animator.GetCurrentAnimatorStateInfo(0);

        animator.SetFloat("InputX", Input.GetAxis("Horizontal"));
        animator.SetFloat("InputY", Input.GetAxis("Vertical"));
        animator.SetInteger("JumpsLeft", numOfJumps);

        inputX = animator.GetFloat("InputX");
        isGrounded = animator.GetBool("IsGrounded");
    }

    private void Jump()
    {
        if(isGrounded)
        {
            ResetJumps();
            animator.SetTrigger("JumpTrigger");
            numOfJumps--;
        }
        else if(numOfJumps > 0)
        {
            animator.SetTrigger("JumpTrigger");
            numOfJumps--;
        }
    }

    private void ResetJumps()
    {
        numOfJumps = MAX_JUMPS;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if the player collides with the ground, isGrounded is true
        if (collision.transform.tag == "Ground")
        {
            animator.SetBool("isGrounded", true);
            //anim.SetBool("isFalling", false);
        }

        // Ignores collision between player colliders
        if (collision.transform.tag == "Player")
        {
            Physics2D.IgnoreCollision(collision.collider, GetComponent<BoxCollider2D>());
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        //if the player stops colliding with the ground, isGrounded is false
        if (collision.transform.tag == "Ground")
        {
            animator.SetBool("isGrounded", false);
        }
    }
}
