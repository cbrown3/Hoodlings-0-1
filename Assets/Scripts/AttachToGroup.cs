﻿using UnityEngine;
using UnityEngine.UI;

public class AttachToGroup : MonoBehaviour
{
    public ToggleGroup hoodlingGroup;
    public ToggleGroup stringGroup;

    private void OnEnable()
    {
        foreach(Transform child in transform)
        {
            if(child.name.Contains("HoodlingToggle"))
            {
                child.GetComponent<Toggle>().group = hoodlingGroup;
            }
            else if (child.name.Contains("StringToggle"))
            {
                child.GetComponent<Toggle>().group = stringGroup;
            }
        }
    }
}
