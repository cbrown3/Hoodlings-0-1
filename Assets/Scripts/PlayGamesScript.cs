﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.UI;

public class PlayGamesScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
        PlayGamesPlatform.DebugLogEnabled = false;
        Social.localUser.Authenticate(success =>
        {
            if (success)
            {
                Debug.Log("successful sign-in!");
            }
            else
            {
                Debug.LogError("Unable to sign in to Google Play Games Services");
            }
        });
#endif
    }

    #region Leaderboards
    public static void AddScoreToLeaderboard(long score)
    {
        Social.ReportScore(score, GPGSIds.leaderboard_hoodlings_leaderboard, (bool success) => 
        {
            if (success)
            {
                Debug.Log("successfully reported score!");
            }
            else
            {
                Debug.LogError("Unable to post new score to leaderboard");
            }
        });
    }

    public static void ShowLeaderboardsUI()
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_hoodlings_leaderboard);
        print("showing Leaderboards");
    }
    #endregion
}
