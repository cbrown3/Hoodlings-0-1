﻿using UnityEngine;
using UnityEngine.UI;

public class TriggerStringPopup : MonoBehaviour
{
    public GameObject buyPopupPrefab;
    public GameObject cantBuyPopupPrefab;
    private Text stringColorText;
    private Text stringCostText;

    public string stringColor;
    public int stringCost;

    private Button button;

    private void Start()
    {
        button = GetComponent<Button>();
    }

    private void Update()
    {
        /*if (GameObject.Find("StoreScreen"))
        {
            if(!stringPopup)
            {
                stringPopup = GameObject.FindGameObjectWithTag("StringPopup");
                stringPopup.SetActive(false);
            }
        }*/
    }

    public void TriggerPopup()
    {
        if(PlayerPrefs.GetInt("Coins") < stringCost)
        {
            Instantiate(cantBuyPopupPrefab, GameObject.Find("StoreScreen").transform);
        }
        else
        {
            Instantiate(buyPopupPrefab, GameObject.Find("StoreScreen").transform);

            stringColorText = GameObject.Find("StringColorText").GetComponent<Text>();
            stringCostText = GameObject.Find("StringCostText").GetComponent<Text>();

            stringCostText.text = stringCost.ToString() + " Coins?";
            stringColorText.text = stringColor + " String";
        }
    }
}
