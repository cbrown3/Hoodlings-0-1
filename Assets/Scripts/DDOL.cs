﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DDOL : MonoBehaviour
{
    public Camera loadingCamera;
    public Text loadingText;
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        SceneManager.LoadScene(1, LoadSceneMode.Additive);

        SceneManager.sceneLoaded += MainMenuLoaded;
    }

    private void MainMenuLoaded(Scene arg0, LoadSceneMode arg1)
    {
        loadingCamera.enabled = false;

        loadingText.enabled = false;

        //SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(1));
    }
}
