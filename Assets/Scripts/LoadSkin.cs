﻿using UnityEngine;

public class LoadSkin : MonoBehaviour
{
    public Animator hoodlingAnimator;

    private int loadedSkinIndex;

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("LoadedSkin"))
        {
            PlayerPrefs.SetInt("LoadedSkin", 1);
        }

        loadedSkinIndex = PlayerPrefs.GetInt("LoadedSkin");

        switch (loadedSkinIndex)
        {
            case 1:
                //blank
                hoodlingAnimator.runtimeAnimatorController = Resources.Load("Animation/BlankHoodlingController") as RuntimeAnimatorController;
                break;
            case 2:
                //regular
                hoodlingAnimator.runtimeAnimatorController = Resources.Load("Animation/RegularHoodlingController") as RuntimeAnimatorController;
                break;
            case 3:
                //rainbow
                hoodlingAnimator.runtimeAnimatorController = Resources.Load("Animation/RainbowHoodlingController") as RuntimeAnimatorController;
                break;
            case 4:
                //unicorn
                hoodlingAnimator.runtimeAnimatorController = Resources.Load("Animation/UnicornHoodlingController") as RuntimeAnimatorController;
                break;
            case 5:
                //zombie
                hoodlingAnimator.runtimeAnimatorController = Resources.Load("Animation/ZombieHoodlingController") as RuntimeAnimatorController;
                break;
            case 6:
                //gold
                hoodlingAnimator.runtimeAnimatorController = Resources.Load("Animation/GoldHoodlingController") as RuntimeAnimatorController;
                break;
        }
    }
}
