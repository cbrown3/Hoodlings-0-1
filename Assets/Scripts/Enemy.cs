﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    Vector3 enemyPosition, currentPosition;

    public enum State
    {
        Up,
        Down
    }

    public State currentState;
    // Start is called before the first frame update
    void Start()
    {
        enemyPosition = new Vector3();
        currentPosition = new Vector3();
        enemyPosition = transform.position;
        currentPosition = enemyPosition;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(currentState==State.Up)
        {
            currentPosition = new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z);
            transform.position = currentPosition;
            if (currentPosition.y > enemyPosition.y + 1f)
            {
                currentState = State.Down;
            }
        }
        else
        {
            currentPosition = new Vector3(transform.position.x, transform.position.y - 0.1f, transform.position.z);
            transform.position = currentPosition;
            if (currentPosition.y < enemyPosition.y - 1f)
            {
                currentState = State.Up;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            HealthManager.Health--;
            ScoreManager.score -= 50;
            if (HealthManager.Health <= 0)
            {
                GameStateManager.CurrentStatus = GameStateManager.PlayerStatus.Lost;
            }

        }
    }
}
