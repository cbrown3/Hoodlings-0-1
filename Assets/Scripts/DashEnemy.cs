﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DashEnemy : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if(AnimCharController.currentState == AnimCharController.State.DashLeft  ||
                AnimCharController.currentState == AnimCharController.State.DashRight ||
                AnimCharController.currentState == AnimCharController.State.CrystalDash)
            {
                GameObject parent = transform.parent.gameObject;
                ScoreManager.score += 50;
                Destroy(gameObject);
                Destroy(parent);
            }
        }
    }
}
