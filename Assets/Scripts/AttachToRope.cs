﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachToRope : MonoBehaviour
{
    private bool swing = false;
    private void Update()
    {
        if(swing)
            transform.Rotate(Vector3.back * 20 * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        swing = true;
    }
}
