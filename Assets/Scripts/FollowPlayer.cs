﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform target;
    // Update is called once per frame
    void Update()
    {
        if(target.position.y <= 0)
        {
            transform.position = new Vector3(target.position.x, 0, transform.position.z);
        }
        else if(target.position.y > 20)
        {
            transform.position = new Vector3(target.position.x, 20, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
        }
    }
}
