﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoodlingStorePopup : MonoBehaviour
{
    public GameObject hoodlingPopupPrefab;
    // Start is called before the first frame update
    public void TriggerPopup()
    {
        Instantiate(hoodlingPopupPrefab, GameObject.Find("StoreScreen").transform);
    }
}
