﻿using UnityEngine;

public class LoadLevel : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            InfiniteLevelLoader levelLoader = GameObject.Find("GameManager").GetComponent<InfiniteLevelLoader>();
            levelLoader.LoadLevel();
        }
    }
}
