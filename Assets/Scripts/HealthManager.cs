﻿using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    public Image healthBar;
    public Sprite[] healthLevels;

    public static int Health { get; set; } = 3;

    // Update is called once per frame
    void Update()
    {
        switch(Health)
        {
            case 3:
                healthBar.sprite = healthLevels[3];
                break;
            case 2:
                healthBar.sprite = healthLevels[2];
                break;
            case 1:
                healthBar.sprite = healthLevels[1];
                break;
            case 0:
                healthBar.sprite = healthLevels[0];
                break;

            default:
                healthBar.sprite = healthLevels[3];
                break;
        }
    }
}
