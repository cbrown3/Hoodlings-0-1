﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneScript : MonoBehaviour
{
    public void LoadScene(int index)
    {
        StartCoroutine(Loading(index));
    }

    private IEnumerator Loading(int index)
    {
        //Return to Main Menu or Skin Menu
        if (index == 1 || index == 3)
        {
            SceneManager.UnloadSceneAsync(2);
            SceneManager.LoadScene(1, LoadSceneMode.Additive);
        }
        //Play Game
        else if (index == 2)
        {
            SceneManager.UnloadSceneAsync(1);
            SceneManager.LoadScene(2, LoadSceneMode.Additive);
        }
        //Retry
        else if(index == 4)
        {
            SceneManager.UnloadSceneAsync(2);
            SceneManager.LoadScene(2, LoadSceneMode.Additive);
        }

        //Return to Main Menu
        if (index == 1)
        {
            ScoreManager.loadingStore = false;
            Time.timeScale = 1;

            //SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(index));
        }
        //Play Game
        else if (index == 2)
        {
            ScoreManager.score = 0;
            HealthManager.Health = 3;
            Time.timeScale = 1;

            //SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(index));
        }
        //Go to Skin Menu
        else if (index == 3)
        {
            ScoreManager.loadingStore = true;
            Time.timeScale = 1;
            GameObject.Find("WelcomeScreen").SetActive(false);
            GameObject.Find("StoreScreen").SetActive(true);

            //SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(1));
        }
        //Retry
        else if(index == 4)
        {
            ScoreManager.score = 0;
            HealthManager.Health = 3;
            Time.timeScale = 1;

            //SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(2));
        }

        yield return null;
    }
}
