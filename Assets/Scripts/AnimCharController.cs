﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
public class AnimCharController : MonoBehaviour
{
    public float groundSpeed = 12;
    public float dashSpeed = 25;
    public float jumpSpeed = 12;
    public float maxVelocity = 12;


    public enum State
    {
        Stop,
        DashLeft,
        DashRight,
        CrystalDash,
        Run,
        Swing,
        JumpForward,
        JumpUp,
        Fall
    }

    public static State currentState;

    public DashCrystalBar dashBar;

    [SerializeField]
    private State stateSerializationHelper;

    private Touch firstTouch;
    private Rect screenRect;
    private Animator animator;
    private Rigidbody2D rigid;
    private DistanceJoint2D joint;
    private new ParticleSystem particleSystem;
    private float savedMaxVelocity;
    private int animTimer = 10;
    private bool spacePressed = false;
    private bool xPressed = false;
    private bool zPressed = false;
    private bool jumpPressed = false;
    private bool canDash = false;
    private bool canCrystalDash = false;
    private bool eventInvoked = false;

    private const string aStopAnim = "Stop";
    private const string aDashLeftAnim = "DashLeft";
    private const string aCrystalDashAnim = "CrystalDash";
    private const string aDashRightAnim = "DashRight";
    private const string aRunAnim = "Run";
    private const string aSwingAnim = "Swing";
    private const string aJumpForwardAnim = "JumpForward";
    private const string aJumpUpAnim = "JumpUp";
    private const string aFallAnim = "Fall";


    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody2D>();
        joint = GetComponent<DistanceJoint2D>();
        savedMaxVelocity = maxVelocity;
        particleSystem = GetComponent<ParticleSystem>();
        particleSystem.Stop();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            spacePressed = true;
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            if (currentState != State.Swing)
            {
                xPressed = true;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Z))
        {
            if (currentState != State.Swing)
            {
                zPressed = true;
            }
        }

        stateSerializationHelper = currentState;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ContinueState();
        if (rigid.velocity.x > maxVelocity && (currentState != State.DashLeft || currentState != State.DashRight))
        {
            rigid.velocity = new Vector2(maxVelocity, rigid.velocity.y);
        }
    }

    /* private void SetOrKeepState(State state)
     {
         if(currentState == state)
         {
             EnterState(state);
         }
     }*/

    private void EnterState(State stateEntered)
    {
        switch (stateEntered)
        {
            case State.Stop:
                animator.Play(aStopAnim);
                rigid.velocity = Vector2.zero;
                rigid.constraints = RigidbodyConstraints2D.FreezePositionX;
                jumpPressed = false;
                break;

            case State.DashLeft:
                animator.Play(aDashLeftAnim);
                rigid.AddForce(new Vector2(-dashSpeed, 0), ForceMode2D.Impulse);
                animTimer = 10;
                canDash = false;
                break;

            case State.DashRight:
                animator.Play(aDashRightAnim);
                particleSystem.Play();
                maxVelocity = dashSpeed;
                rigid.AddForce(new Vector2(dashSpeed, 0), ForceMode2D.Impulse);
                animTimer = 15;
                canDash = false;
                break;

            case State.CrystalDash:
                animator.Play(aCrystalDashAnim);
                particleSystem.Play();
                maxVelocity = dashSpeed;
                rigid.AddForce(new Vector2(dashSpeed, 0), ForceMode2D.Impulse);
                animTimer = 15;
                dashBar.currentMaxValue = 0;
                canCrystalDash = false;
                break;

            case State.Run:
                animator.Play(aRunAnim);
                rigid.velocity.Set(groundSpeed, 0);
                jumpPressed = false;
                break;

            case State.JumpForward:
                animator.Play(aJumpForwardAnim);
                rigid.velocity = Vector2.zero;
                rigid.AddForce(new Vector2(groundSpeed, jumpSpeed), ForceMode2D.Impulse);
                jumpPressed = true;
                break;

            case State.JumpUp:
                animator.Play(aJumpUpAnim);
                rigid.velocity = Vector2.zero;
                rigid.AddForce(new Vector2(0, jumpSpeed), ForceMode2D.Impulse);
                jumpPressed = true;
                break;

            case State.Swing:
                animator.Play(aSwingAnim);
                rigid.freezeRotation = false;
                jumpPressed = false;
                break;

            case State.Fall:
                animator.Play(aFallAnim);
                break;
        }

        currentState = stateEntered;
    }

    private void ContinueState()
    {
        switch (currentState)
        {
            //State when a Hoodling is on a stationary string
            case State.Stop:
#if UNITY_EDITOR
                if (spacePressed)
                {
                    spacePressed = false;
                    EnterState(State.JumpUp);
                }
                if (xPressed)
                {
                    xPressed = false;
                    rigid.constraints = RigidbodyConstraints2D.None;
                    rigid.constraints = RigidbodyConstraints2D.FreezeRotation;
                    EnterState(State.JumpForward);
                }
#endif
                break;

            //State that dashes left
            case State.DashLeft:
                if (animTimer < 1)
                {
                    rigid.velocity = new Vector2(maxVelocity, rigid.velocity.y);
                    EnterState(State.Fall);
                }
#if UNITY_EDITOR
                else if (spacePressed && !jumpPressed)
                {
                    spacePressed = false;
                    EnterState(State.JumpForward);
                }
#endif

                animTimer--;
                break;

            //State that dashes right
            case State.DashRight:
                ScoreManager.score++;
                if (animTimer < 1)
                {
                    maxVelocity = savedMaxVelocity;
                    rigid.velocity = new Vector2(maxVelocity, rigid.velocity.y);
                    particleSystem.Stop();
                    EnterState(State.Fall);
                }
#if UNITY_EDITOR
                else if (spacePressed && !jumpPressed)
                {
                    spacePressed = false;
                    maxVelocity = savedMaxVelocity;
                    particleSystem.Stop();
                    EnterState(State.JumpForward);
                }
#endif

                animTimer--;
                break;

            //State that dashes right
            case State.CrystalDash:
                ScoreManager.score++;
                if (animTimer < 1)
                {
                    maxVelocity = savedMaxVelocity;
                    rigid.velocity = new Vector2(maxVelocity, rigid.velocity.y);
                    particleSystem.Stop();
                    EnterState(State.Fall);
                }
#if UNITY_EDITOR
                else if (spacePressed && !jumpPressed)
                {
                    spacePressed = false;
                    maxVelocity = savedMaxVelocity;
                    particleSystem.Stop();
                    EnterState(State.JumpForward);
                }
#endif

                animTimer--;
                break;

            //Entry state of running on platforms
            case State.Run:
                ScoreManager.score++;
                rigid.AddForce(new Vector2(groundSpeed, 0), ForceMode2D.Impulse);

                if (Mathf.Round(rigid.velocity.y) < 0)
                {
                    EnterState(State.Fall);
                }
#if UNITY_EDITOR
                if (spacePressed && !jumpPressed)
                {
                    spacePressed = false;
                    EnterState(State.JumpForward);
                }
                if (canDash && xPressed && dashBar.currentValue > 0)
                {
                    EnterState(State.CrystalDash);
                    xPressed = false;
                }
#endif
                break;
            //State when a Hoodling is swinging on a rope
            case State.Swing:
                transform.rotation = joint.connectedBody.transform.rotation;
#if UNITY_EDITOR
                if (spacePressed && !jumpPressed)
                {
                    spacePressed = false;
                    transform.rotation = Quaternion.identity;
                    rigid.freezeRotation = true;
                    joint.enabled = false;
                    EnterState(State.JumpForward);
                }
#endif
                break;

            //State when a Hoodling is jumping forward from a platform, falling, or off a string
            case State.JumpForward:
                ScoreManager.score++;
                if (Mathf.Round(rigid.velocity.y) < 0)
                {
                    EnterState(State.Fall);
                }
#if UNITY_EDITOR
                if (canDash && xPressed && dashBar.currentValue > 0)
                {
                    EnterState(State.CrystalDash);
                    xPressed = false;
                }
#endif
                break;

            //State when a Hoodling jumps up a string
            case State.JumpUp:
                if (Mathf.Round(rigid.velocity.y) < 0)
                {
                    EnterState(State.Stop);
                }
#if UNITY_EDITOR
                if (xPressed)
                {
                    xPressed = false;
                    rigid.constraints = RigidbodyConstraints2D.None;
                    rigid.constraints = RigidbodyConstraints2D.FreezeRotation;
                    EnterState(State.JumpForward);
                }
#endif
                break;

            //State when a Hoodling is falling in general
            case State.Fall:
                ScoreManager.score++;
                //if you land on a platform, start running again 
                if (Mathf.Round(rigid.velocity.y) == 0)
                {
                    EnterState(State.Run);
                }
#if UNITY_EDITOR
                if (spacePressed && !jumpPressed)
                {
                    spacePressed = false;
                    EnterState(State.JumpForward);
                }
                if (canDash && xPressed && dashBar.currentValue > 0)
                {
                    EnterState(State.CrystalDash);
                    xPressed = false;
                }
#endif
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "SwingingString")
        {
            if (joint.connectedBody != collision.GetComponent<Rigidbody2D>())
            {
                if (currentState == State.DashRight)
                {
                    maxVelocity = savedMaxVelocity;
                    particleSystem.Stop();
                }
                joint.enabled = true;
                joint.connectedBody = collision.GetComponent<Rigidbody2D>();
                EnterState(State.Swing);
            }
        }

        if (collision.tag == "StationaryString")
        {
            canDash = false;

            if (currentState == State.DashRight)
            {
                maxVelocity = savedMaxVelocity;
                particleSystem.Stop();
            }

            transform.position = collision.transform.position;
            EnterState(State.Stop);
        }

        if (collision.tag == "DashCrystal")
        {
            canCrystalDash = true;
            dashBar.currentMaxValue = 1;
            dashBar.currentTime = Time.fixedTime;

            Destroy(collision.gameObject);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "DashArrow")
        {
            canDash = true;
#if UNITY_EDITOR
            if (xPressed)
            {
                xPressed = false;
                EnterState(State.DashRight);
            }
            else if (zPressed)
            {
                zPressed = false;
                EnterState(State.DashLeft);
            }
#endif
        }

    }

    /*private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "StationaryString")
        {
            collision.GetComponent<BoxCollider2D>().enabled = false;
        }
    }*/

    public void DashRight(bool direction)
    {
        if (!eventInvoked)
        {
            if (canDash)
            {
                eventInvoked = true;

                if (direction)
                {
                    EnterState(State.DashRight);
                }
                else
                {
                    EnterState(State.DashLeft);
                }
            }
        }
        else
        {
            eventInvoked = false;
        }
    }

    public void CrystalDash()
    {
        if (!eventInvoked)
        {
            if (canCrystalDash && dashBar.currentValue > 0)
            {
                eventInvoked = true;

                EnterState(State.CrystalDash);
            }
        }
        else
        {
            eventInvoked = false;
        }
    }

    public void JumpForward()
    {
        if (!eventInvoked)
        {
            if (!jumpPressed)
            {
                eventInvoked = true;

                switch (currentState)
                {
                    case State.DashRight:
                        maxVelocity = savedMaxVelocity;
                        particleSystem.Stop();
                        break;

                    case State.Swing:
                        joint.enabled = false;
                        transform.rotation = Quaternion.identity;
                        rigid.constraints = RigidbodyConstraints2D.FreezeRotation;
                        break;

                    case State.JumpUp:
                    case State.Stop:
                        rigid.constraints = RigidbodyConstraints2D.None;
                        rigid.constraints = RigidbodyConstraints2D.FreezeRotation;
                        break;

                    default:

                        break;
                }

                EnterState(State.JumpForward);

                eventInvoked = false;
            }
        }
        else
        {
            eventInvoked = false;
        }
    }

    public void JumpUp()
    {
        if (!eventInvoked)
        {
            if (currentState == State.JumpUp || currentState == State.Stop)
            {
                eventInvoked = true;

                EnterState(State.JumpUp);

                eventInvoked = false;
            }
        }
        else
        {
            eventInvoked = false;
        }
    }
}