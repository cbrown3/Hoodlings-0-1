﻿using UnityEngine;

public class FallScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GameStateManager.CurrentStatus = GameStateManager.PlayerStatus.Lost;
        }
    }
}
