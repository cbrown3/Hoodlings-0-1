﻿using UnityEngine;

public class BounceEnemy : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameObject parent = transform.parent.gameObject;
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(12, 12), ForceMode2D.Impulse);
            ScoreManager.score += 50;
            Destroy(gameObject);
            Destroy(parent);
        }
    }
}
