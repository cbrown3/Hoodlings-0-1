﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetHighScores : MonoBehaviour
{
    public void HighScoreToggle(int toggle)
    {
        PlayerPrefs.SetInt("HighScores", toggle);
    }
}
