﻿using UnityEngine;

public class TutorialToggle : MonoBehaviour
{
    private void Awake()
    {
        if(!PlayerPrefs.HasKey("Tutorial"))
        {
            PlayerPrefs.SetInt("Tutorial", 1);
        }

        if(PlayerPrefs.GetInt("Tutorial") == 0)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }
    }
}
