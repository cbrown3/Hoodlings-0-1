﻿using UnityEngine;
using UnityEngine.UI;

public class SwitchStoreTab : MonoBehaviour
{
    public GameObject storeViewport;
    public GameObject inventoryViewport;

    public Image button;
    private Toggle toggle;
    
    private void Start()
    {
        toggle = GetComponent<Toggle>();
    }

    // Update is called once per frame
    void Update()
    {
        switch(toggle.isOn)
        {
            case true:
                storeViewport.SetActive(true);
                inventoryViewport.SetActive(false);
                button.CrossFadeAlpha(0, 0, true);
                break;

            case false:
                storeViewport.SetActive(false);
                inventoryViewport.SetActive(true);
                button.CrossFadeAlpha(255, 0, true);
                break;
        }
    }
}
