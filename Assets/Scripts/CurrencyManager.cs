﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CurrencyManager : MonoBehaviour
{
    public Text coinsText;
    public static int currentCoins = 0;
    public static int totalCoins;

    public UnityEvent onBuyStrings; 

    private static CurrencyManager instanceRef;
    // Start is called before the first frame update
    void Awake()
    {
        if(!PlayerPrefs.HasKey("Coins"))
        {
            PlayerPrefs.SetInt("Coins", 0);
        }
        else
        {
            totalCoins = PlayerPrefs.GetInt("Coins");
        }

        currentCoins = PlayerPrefs.GetInt("Coins");

        if (instanceRef == null)
        {
            instanceRef = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }

        SceneManager.sceneLoaded += ResetCoins;
    }

    private void ResetCoins(Scene loadedScene, LoadSceneMode mode)
    {
        if(loadedScene.buildIndex == 2)
        {
            currentCoins = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            if(coinsText == null)
            {
                coinsText = GameObject.Find("CoinsText").GetComponent<Text>();
            }
            coinsText.text = "Coins - " + currentCoins.ToString("00");
        }
        else if(GameObject.Find("StoreScreen"))
        {
            if (coinsText == null)
            {
                coinsText = GameObject.Find("TotalCoins").GetComponent<Text>();
            }
            coinsText.text = "Coins: " + PlayerPrefs.GetInt("Coins").ToString("00");

            if(Input.GetKey(KeyCode.Alpha1))
            {
                PlayerPrefs.SetInt("Coins", totalCoins++);
            }
        }
    }

    public void LoadCoins()
    {
        totalCoins += currentCoins;
        PlayerPrefs.SetInt("Coins", totalCoins);
    }

    private void OnDisable()
    {
        PlayerPrefs.SetInt("Coins", currentCoins);
        PlayerPrefs.Save();
    }
}
