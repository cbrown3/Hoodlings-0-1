﻿using UnityEngine;

public class LoadString : MonoBehaviour
{
    public Sprite[] coloredStrings;

    private GameObject[] stationary;
    private GameObject[] swinging;

    private int loadedStringIndex;

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("LoadedString"))
        {
            PlayerPrefs.SetInt("LoadedString", 0);
        }

        loadedStringIndex = PlayerPrefs.GetInt("LoadedString");

        stationary = GameObject.FindGameObjectsWithTag("StationaryString");
        swinging = GameObject.FindGameObjectsWithTag("SwingingString");
    }

    public void ReloadStrings()
    {
        stationary = GameObject.FindGameObjectsWithTag("StationaryString");
        swinging = GameObject.FindGameObjectsWithTag("SwingingString");

        switch (loadedStringIndex)
        {
            case 0:
                //gray
                foreach (GameObject go in stationary)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[0];
                }
                foreach (GameObject go in swinging)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[0];
                }
                break;
            case 1:
                //blue
                foreach (GameObject go in stationary)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[1];
                }
                foreach (GameObject go in swinging)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[1];
                }
                break;
            case 2:
                //green
                foreach (GameObject go in stationary)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[2];
                }
                foreach (GameObject go in swinging)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[2];
                }
                break;
            case 3:
                //cyan
                foreach (GameObject go in stationary)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[3];
                }
                foreach (GameObject go in swinging)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[3];
                }
                break;
            case 4:
                //orange
                foreach (GameObject go in stationary)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[4];
                }
                foreach (GameObject go in swinging)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[4];
                }
                break;
            case 5:
                //yellow
                foreach (GameObject go in stationary)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[5];
                }
                foreach (GameObject go in swinging)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[5];
                }
                break;
            case 6:
                //purple
                foreach (GameObject go in stationary)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[6];
                }
                foreach (GameObject go in swinging)
                {
                    go.GetComponent<SpriteRenderer>().sprite = coloredStrings[6];
                }
                break;
        }
    }
}
