﻿using UnityEngine;

public class UnloadLevel : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            InfiniteLevelLoader levelLoader = GameObject.FindWithTag("GameManager").GetComponent<InfiniteLevelLoader>();
            levelLoader.RemoveLevel();
        }
    }
}
