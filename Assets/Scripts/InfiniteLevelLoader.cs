﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class InfiniteLevelLoader : MonoBehaviour
{
    public GameObject[] allLevels;
    public GameObject[] firstLevels;
    public UnityEvent OnLoadLevel;

    private List<GameObject> loadedLevels;
    private float[] levelWeight;
    private float highestWeight;
    private Vector3 spawnPoint;
    // Start is called before the first frame update
    void Start()
    {
        loadedLevels = new List<GameObject>();
        spawnPoint = new Vector3(2f, 0, 0);
        levelWeight = new float[allLevels.Length];

        //set level weight

        for(int i = 0; i < allLevels.Length; i++)
        {
            SetWeight(i, (i + 1) * 10);
        }

        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(2));

        LoadFirstLevel();

        LoadLevel();
    }

    private void CalculateHighestWeight()
    {
        highestWeight = 0;
        for (int i = 0; i < levelWeight.Length; i++)
        {
            if (highestWeight < levelWeight[i])
            {
                highestWeight = levelWeight[i];
            }
        }
    }

    private void SetWeight(int levelIndex, int newWeight)
    {
        levelWeight[levelIndex] = newWeight;

        CalculateHighestWeight();
    }

    private void IncreaseDifficulty()
    {

    }

    private void LoadFirstLevel()
    {
        GameObject addedLevel = Instantiate(SelectFirstLevel(), spawnPoint, Quaternion.identity);

        Destroy(addedLevel.GetComponent<UnloadLevel>());

        loadedLevels.Add(addedLevel);

        spawnPoint.x += 110;
    }

    public void LoadLevel()
    {
        GameObject addedLevel = Instantiate(SelectLevel(), spawnPoint, Quaternion.identity);

        loadedLevels.Add(addedLevel);

        spawnPoint.x += 110;

        OnLoadLevel.Invoke();
    }

    public void RemoveLevel()
    {
        GameObject first = loadedLevels[0];
        loadedLevels.Remove(first);
        Destroy(first);
    }

    private GameObject SelectFirstLevel()
    {
        int index = Random.Range(0, firstLevels.Length);
        if (!loadedLevels.Contains(firstLevels[index]))
        {
            return firstLevels[index];
        }
        return SelectFirstLevel();
    }

    private GameObject SelectLevel()
    {
        float randomNumber = Random.Range(0, highestWeight);

        for(int i = 0; i < levelWeight.Length; i++)
        {
            if(randomNumber < levelWeight[i])
            {
                if(!loadedLevels.Contains(allLevels[i]))
                {
                    return allLevels[i];
                }
            }
        }

        return SelectLevel();
    }
}
