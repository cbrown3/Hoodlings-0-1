﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadShop : MonoBehaviour
{
    private void OnEnable()
    {
        GameObject.Find("UnlockManager").GetComponent<UnlockManager>().LoadShopInv();
    }
}
