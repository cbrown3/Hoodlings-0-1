﻿using UnityEngine;
using UnityEngine.UI;

public class DashCrystalBar : MonoBehaviour
{
    private Image dashBar;
    public float currentMaxValue = 0;
    public float currentValue = 0;
    public float currentTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        dashBar = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        CalculateBar();
        dashBar.fillAmount = currentValue;
    }

    private void CalculateBar()
    {
        currentValue = Mathf.Clamp(currentMaxValue - (0.5f * (Time.fixedTime - currentTime)), 0, 1);
    }
}
