﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class CheckCodes : MonoBehaviour
{
    public Text codeText;
    public TextAsset codeFile;
    private string[] lines;

    public GameObject storeContent;
    public GameObject inventoryContent;

    public GameObject invalidCodePrefab;
    public GameObject validCodePrefab;

    void Start()
    {
        lines = codeFile.text.Split("\n"[0]);
    }

    public void ValidateCode()
    {
        string code = codeText.text.ToUpper();
        code = code.Insert(4, "-");
        code = code.Insert(9, "-");

        for (int i = 0; i < lines.Length; i++)
        {
            if(lines[i].Replace("\r","") == code)
            {
                if(i < 3045)
                {
                    PlayerPrefs.SetInt("RegularHoodling", 1);

                    Instantiate(validCodePrefab, GameObject.Find("CodeScreen").transform);
                    GameObject.Find("HoodlingText").GetComponent<Text>().text = "Regular Hoodling!";
                    break;
                }
                else if(i < 6090)
                {
                    PlayerPrefs.SetInt("UnicornHoodling", 1);

                    Instantiate(validCodePrefab, GameObject.Find("CodeScreen").transform);
                    GameObject.Find("HoodlingText").GetComponent<Text>().text = "Unicorn Hoodling!";
                    break;
                }
                else if(i < 6438)
                {
                    PlayerPrefs.SetInt("GoldHoodling", 1);

                    Instantiate(validCodePrefab, GameObject.Find("CodeScreen").transform);
                    GameObject.Find("HoodlingText").GetComponent<Text>().text = "Gold Hoodling!";
                    break;
                }
                else if(i < 9483)
                {
                    PlayerPrefs.SetInt("ZombieHoodling", 1);

                    Instantiate(validCodePrefab, GameObject.Find("CodeScreen").transform);
                    GameObject.Find("HoodlingText").GetComponent<Text>().text = "Zombie Hoodling!";
                    break;
                }
                else if(i < 12528)
                {
                    PlayerPrefs.SetInt("RainbowHoodling", 1);

                    Instantiate(validCodePrefab, GameObject.Find("CodeScreen").transform);
                    GameObject.Find("HoodlingText").GetComponent<Text>().text = "Rainbow Hoodling!";
                    break;
                }
            }
            else if(i == lines.Length - 1)
            {
                Instantiate(invalidCodePrefab, GameObject.Find("CodeScreen").transform);
            }
        }
    }
}
