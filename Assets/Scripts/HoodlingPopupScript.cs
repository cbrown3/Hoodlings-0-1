﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoodlingPopupScript : MonoBehaviour
{
    // Start is called before the first frame update
    public void LaunchWebsiteLink()
    {
        Debug.Log("Launching website link...");
    }

    public void ClosePopup()
    {
        Destroy(gameObject);
    }
}
