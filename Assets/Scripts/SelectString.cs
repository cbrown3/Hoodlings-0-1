﻿using UnityEngine;
using UnityEngine.UI;

public class SelectString : MonoBehaviour
{
    public void SetStringPref(int index)
    {
        PlayerPrefs.SetInt("LoadedString", index);
    }
}
