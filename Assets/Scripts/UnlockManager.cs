﻿using UnityEngine;

public class UnlockManager : MonoBehaviour
{
    [SerializeField]
    private GameObject storeContent;
    [SerializeField]
    private GameObject inventoryContent;

    private GameObject go;

    //private static UnlockManager instanceRef;
    void Awake()
    {
        /*if (instanceRef == null)
        {
            instanceRef = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
        */
        CheckUnlocks();
    }

    private void CheckUnlocks()
    {
        if(!PlayerPrefs.HasKey("RegularHoodling"))
        {
            PlayerPrefs.SetInt("RegularHoodling", 0);
        }
        if (!PlayerPrefs.HasKey("RainbowHoodling"))
        {
            PlayerPrefs.SetInt("RainbowHoodling", 0);
        }
        if (!PlayerPrefs.HasKey("UnicornHoodling"))
        {
            PlayerPrefs.SetInt("UnicornHoodling", 0);
        }
        if (!PlayerPrefs.HasKey("ZombieHoodling"))
        {
            PlayerPrefs.SetInt("ZombieHoodling", 0);
        }
        if (!PlayerPrefs.HasKey("GoldHoodling"))
        {
            PlayerPrefs.SetInt("GoldHoodling", 0);
        }
        if (!PlayerPrefs.HasKey("BlueString"))
        {
            PlayerPrefs.SetInt("BlueString", 0);
        }
        if (!PlayerPrefs.HasKey("GreenString"))
        {
            PlayerPrefs.SetInt("GreenString", 0);
        }
        if (!PlayerPrefs.HasKey("OrangeString"))
        {
            PlayerPrefs.SetInt("OrangeString", 0);
        }
        if (!PlayerPrefs.HasKey("YellowString"))
        {
            PlayerPrefs.SetInt("YellowString", 0);
        }
        if (!PlayerPrefs.HasKey("PurpleString"))
        {
            PlayerPrefs.SetInt("PurpleString", 0);
        }
    }

    public void LoadShopInv()
    {
        if(storeContent == null)
        {
            storeContent = GameObject.FindGameObjectWithTag("StoreContent");
        }

        if(inventoryContent == null)
        {
            inventoryContent = GameObject.FindGameObjectWithTag("InventoryContent");
        }

        foreach (Transform child in storeContent.transform)
        {
            if(child.name.Contains("Button"))
            {
                Destroy(child.gameObject);
            }
        }
        foreach(Transform child in inventoryContent.transform)
        {
            if(child.name.Contains("Blank") || child.name.Contains("Gray"))
            {
                continue;
            }
            if(child.name.Contains("Toggle"))
            {
                Destroy(child.gameObject);
            }
        }

        if (PlayerPrefs.GetInt("RegularHoodling") == 0)
        {
            go = Resources.Load("UI/RegularHoodlingButton") as GameObject;
            Instantiate(go, storeContent.transform);
        }
        else
        {
            go = Resources.Load<GameObject>("UI/RegularHoodlingToggle");
            Instantiate(go, inventoryContent.transform);
        }
        if (PlayerPrefs.GetInt("RainbowHoodling") == 0)
        {
            go = Resources.Load<GameObject>("UI/RainbowHoodlingButton");
            Instantiate(go, storeContent.transform);
        }
        else
        {
            go = Resources.Load<GameObject>("UI/RainbowHoodlingToggle");
            Instantiate(go, inventoryContent.transform);
        }
        if (PlayerPrefs.GetInt("UnicornHoodling") == 0)
        {
            go = Resources.Load<GameObject>("UI/UnicornHoodlingButton");
            Instantiate(go, storeContent.transform);
        }
        else
        {
            go = Resources.Load<GameObject>("UI/UnicornHoodlingToggle");
            Instantiate(go, inventoryContent.transform);
        }
        if (PlayerPrefs.GetInt("ZombieHoodling") == 0)
        {
            go = Resources.Load<GameObject>("UI/ZombieHoodlingButton");
            Instantiate(go, storeContent.transform);
        }
        else
        {
            go = Resources.Load<GameObject>("UI/ZombieHoodlingToggle");
            Instantiate(go, inventoryContent.transform);
        }
        if (PlayerPrefs.GetInt("GoldHoodling") == 0)
        {
            go = Resources.Load<GameObject>("UI/GoldHoodlingButton");
            Instantiate(go, storeContent.transform);
        }
        else
        {
            go = Resources.Load<GameObject>("UI/GoldHoodlingToggle");
            Instantiate(go, inventoryContent.transform);
        }
        /*if (PlayerPrefs.GetInt("GrayString") == 0)
        {
            go = Resources.Load<GameObject>("UI/GrayStringButton");
            Instantiate(go, storeContent.transform);
        }
        else
        {
            go = Resources.Load<GameObject>("UI/GrayStringToggle");
            Instantiate(go, inventoryContent.transform);
        }*/
        if (PlayerPrefs.GetInt("BlueString") == 0)
        {
            go = Resources.Load<GameObject>("UI/BlueStringButton");
            Instantiate(go, storeContent.transform);
        }
        else
        {
            go = Resources.Load<GameObject>("UI/BlueStringToggle");
            Instantiate(go, inventoryContent.transform);
        }
        if (PlayerPrefs.GetInt("GreenString") == 0)
        {
            go = Resources.Load<GameObject>("UI/GreenStringButton");
            Instantiate(go, storeContent.transform);
        }
        else
        {
            go = Resources.Load<GameObject>("UI/GreenStringToggle");
            Instantiate(go, inventoryContent.transform);
        }
        if (PlayerPrefs.GetInt("OrangeString") == 0)
        {
            go = Resources.Load<GameObject>("UI/OrangeStringButton");
            Instantiate(go, storeContent.transform);
        }
        else
        {
            go = Resources.Load<GameObject>("UI/OrangeStringToggle");
            Instantiate(go, inventoryContent.transform);
        }
        if (PlayerPrefs.GetInt("YellowString") == 0)
        {
            go = Resources.Load<GameObject>("UI/YellowStringButton");
            Instantiate(go, storeContent.transform);
        }
        else
        {
            go = Resources.Load<GameObject>("UI/YellowStringToggle");
            Instantiate(go, inventoryContent.transform);
        }
        if (PlayerPrefs.GetInt("PurpleString") == 0)
        {
            go = Resources.Load<GameObject>("UI/PurpleStringButton");
            Instantiate(go, storeContent.transform);
        }
        else
        {
            go = Resources.Load<GameObject>("UI/PurpleStringToggle");
            Instantiate(go, inventoryContent.transform);
        }
    }
}
