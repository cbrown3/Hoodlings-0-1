﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchScreens : MonoBehaviour
{
    public GameObject screenToActivate;
    public GameObject screenToDeactivate;

    public void ScreenSwitch()
    {
        screenToActivate.SetActive(true);
        screenToDeactivate.SetActive(false);
    }
}
