﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour
{
    public GameObject[] objectsToDeactivate;

    public GameObject LoseScreen;

    public enum PlayerStatus
    {
        Playing,
        Lost
    }
    public static PlayerStatus CurrentStatus;

    private PlayerStatus oldStatus;

    private bool[] isPaused;

    public void Start()
    {
        CurrentStatus = PlayerStatus.Playing;

        oldStatus = CurrentStatus;

        LoseScreen.SetActive(false);

        isPaused = new bool[2];

        for (int i = 0; i < 2; i++)
        {
            isPaused[i] = objectsToDeactivate[i].activeInHierarchy;
        }
    }

    private void Update()
    {
        if(oldStatus != CurrentStatus &&
            CurrentStatus == PlayerStatus.Lost)
        {
            Time.timeScale = 0;
            LoseScreen.SetActive(true);
        }

        oldStatus = CurrentStatus;
    }

    public void Pause()
    {
        if(CurrentStatus != PlayerStatus.Lost)
        {
            for (int i = 0; i < 2; i++)
            {
                isPaused[i] = !isPaused[i];
                objectsToDeactivate[i].SetActive(isPaused[i]);
            }

            Time.timeScale = isPaused[0] == true ? 0 : 1;
        }
    }
}
