﻿using System.Collections;
using UnityEngine;

public class RopeMove : MonoBehaviour
{
    private GameObject player;
    private Rigidbody2D rigid;
    public float swingingSpeed = 10;
    public float swingDelay = 1;
    private bool swingRight = true;
    public bool IsSwinging { get; set; } = false;

    private void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        /*if(IsSwinging)
        {
            player.transform.position = transform.position;
        }*/
    }

    IEnumerator SwingingForce()
    {
        while(IsSwinging)
        {
            rigid.velocity = Vector2.zero;
            rigid.angularVelocity = 0;
            if(swingRight)
            {
                rigid.AddForce(Vector2.right * swingingSpeed, ForceMode2D.Impulse);
                swingRight = false;
            }
            else
            {
                rigid.AddForce(Vector2.left * swingingSpeed, ForceMode2D.Impulse);
                swingRight = true;
            }

            yield return new WaitForSeconds(swingDelay);
        }
    }

    /*private void SwingingForce()
    {
        float timer = 0;
        while (IsSwinging)
        {
            if(timer <= 0)
            {
                swingRight = !swingRight;
                timer = swingDelay;
            }
            else
            {
                timer -= Time.deltaTime;

                continue;
            }

            if (swingRight)
            {
                rigid.velocity.Set(0, 0);
                rigid.AddForce(new Vector2(swingingSpeed, 0), ForceMode2D.Impulse);
            }
            else
            {
                rigid.velocity.Set(0, 0);
                rigid.AddForce(new Vector2(-swingingSpeed, 0), ForceMode2D.Impulse);
            }
        }
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            player = collision.gameObject;
            //GetComponent<DistanceJoint2D>().connectedBody = player.GetComponent<Rigidbody2D>();
            IsSwinging = true;
            StartCoroutine(SwingingForce());
        }
    }
}
