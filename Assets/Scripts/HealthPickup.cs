﻿using System.Collections;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(HealthManager.Health < 3)
        {
            HealthManager.Health++;
        }
        Destroy(gameObject);
    }
}
