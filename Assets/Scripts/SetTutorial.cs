﻿using UnityEngine;

public class SetTutorial : MonoBehaviour
{
    public void TutorialSwitch(int toggle)
    {
        PlayerPrefs.SetInt("Tutorial", toggle);
    }
}
