﻿using UnityEngine;

public class Spikes : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            HealthManager.Health--;
            ScoreManager.score -= 50;
            if (HealthManager.Health <= 0)
            {
                GameStateManager.CurrentStatus = GameStateManager.PlayerStatus.Lost;
            }

        }
    }
}
