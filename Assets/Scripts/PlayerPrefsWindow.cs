﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PlayerPrefsWindow : EditorWindow
{
    [MenuItem ("Window/Debugging Windows/PlayerPrefs Window")]
    public static void ShowWindow()
    {
        GetWindow<PlayerPrefsWindow>();
    }

    private void OnGUI()
    {
        EditorGUILayout.LabelField("All PlayerPrefs", EditorStyles.boldLabel);

        EditorGUI.indentLevel++;
        EditorGUILayout.LabelField("Coins", EditorStyles.boldLabel);

        EditorGUI.BeginDisabledGroup(true);
        EditorGUIUtility.labelWidth = 150;
        EditorGUI.indentLevel++;
        EditorGUILayout.IntField("Player's Coins",
            PlayerPrefs.GetInt("Coins"));
        EditorGUI.indentLevel--;
        EditorGUI.EndDisabledGroup();


        EditorGUILayout.LabelField("Loaded Skin and Strings", EditorStyles.boldLabel);

        EditorGUI.BeginDisabledGroup(true);
        EditorGUIUtility.labelWidth = 150;
        EditorGUI.indentLevel++;
        EditorGUILayout.IntField("Loaded Skin",
            PlayerPrefs.GetInt("LoadedSkin"));
        EditorGUILayout.IntField("Loaded String",
            PlayerPrefs.GetInt("LoadedString"));

        EditorGUI.indentLevel--;
        EditorGUI.EndDisabledGroup();


        EditorGUILayout.LabelField("Hoodling and String Unlocks", EditorStyles.boldLabel);

        EditorGUI.BeginDisabledGroup(true);
        EditorGUIUtility.labelWidth = 200;
        EditorGUI.indentLevel++;
        EditorGUILayout.IntField("Regular Hoodling Unlocked",
            PlayerPrefs.GetInt("RegularHoodling"));
        EditorGUILayout.IntField("Rainbow Hoodling Unlocked",
            PlayerPrefs.GetInt("RainbowHoodling"));
        EditorGUILayout.IntField("Unicorn Hoodling Unlocked",
            PlayerPrefs.GetInt("UnicornHoodling"));
        EditorGUILayout.IntField("Zombie Hoodling Unlocked",
            PlayerPrefs.GetInt("ZombieHoodling"));
        EditorGUILayout.IntField("Gold Hoodling Unlocked",
            PlayerPrefs.GetInt("GoldHoodling"));
        EditorGUILayout.IntField("Blue String Unlocked",
            PlayerPrefs.GetInt("BlueString"));
        EditorGUILayout.IntField("Green String Unlocked",
            PlayerPrefs.GetInt("GreenString"));
        EditorGUILayout.IntField("Orange String Unlocked",
            PlayerPrefs.GetInt("OrangeString"));
        EditorGUILayout.IntField("Yellow String Unlocked",
            PlayerPrefs.GetInt("YellowString"));
        EditorGUILayout.IntField("Purple String Unlocked",
            PlayerPrefs.GetInt("PurpleString"));
        EditorGUI.indentLevel--;
        EditorGUI.EndDisabledGroup();


        EditorGUILayout.LabelField("Current Highscores", EditorStyles.boldLabel);

        EditorGUI.BeginDisabledGroup(true);
        EditorGUIUtility.labelWidth = 150;
        EditorGUI.indentLevel++;
        for(int i = 0; i < 10; i++)
        {
            EditorGUILayout.TextField("High Score #" + (i+1) + " Name",
                PlayerPrefs.GetString(i + "HSName"));
            EditorGUILayout.IntField("High Score #" + (i+1) + " Score",
                PlayerPrefs.GetInt(i + "HScore"));
        }
        EditorGUI.indentLevel--;
        EditorGUI.EndDisabledGroup();


        EditorGUILayout.LabelField("Settings", EditorStyles.boldLabel);

        EditorGUI.BeginDisabledGroup(true);
        EditorGUIUtility.labelWidth = 150;
        EditorGUI.indentLevel++;
        EditorGUILayout.IntField("HighScore Toggle",
            PlayerPrefs.GetInt("Highscores"));
        EditorGUILayout.IntField("Tutorial Toggle",
            PlayerPrefs.GetInt("Tutorial"));

        EditorGUI.indentLevel--;
        EditorGUI.EndDisabledGroup();
    }
}
#endif