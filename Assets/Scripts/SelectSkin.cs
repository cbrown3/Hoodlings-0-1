﻿using UnityEngine;
using UnityEngine.UI;

public class SelectSkin : MonoBehaviour
{
    public void SetSkinPref(int index)
    {
        PlayerPrefs.SetInt("LoadedSkin", index);
    }
}
