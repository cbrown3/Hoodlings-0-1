﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    public static int score = 0;
    public static bool loadingStore = false;

    private GameObject storeScreen;

    private Text scoreText;
    private Text finalScoreText;
    private static ScoreManager instanceRef;
    // Start is called before the first frame update
    void Awake()
    {
        if (instanceRef == null)
        {
            instanceRef = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }

        SceneManager.sceneLoaded += LoadObjects;

        LoadObjects(SceneManager.GetSceneByBuildIndex(1), LoadSceneMode.Additive);
    }

    public void OpenShopScreen()
    {
        //loadingStore = true;
    }

    public void ManageScore(Scene unloadedScene)
    {
        AddScore();
        scoreText = null;
    }

    public void LoadObjects(Scene loadedScene, LoadSceneMode mode)
    {
        if(loadedScene.buildIndex == 2)
        {
            scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
            finalScoreText = GameObject.Find("HSText").GetComponent<Text>();
        }
        else if(loadedScene.buildIndex == 1)
        {
            storeScreen = GameObject.FindWithTag("StoreScreen");
            if(!loadingStore)
            {
                if(storeScreen != null)
                {
                    storeScreen.SetActive(false);
                }
            }
            else
            {
                GameObject.FindGameObjectWithTag("WelcomeScreen").SetActive(false);
                GameObject.FindGameObjectWithTag("StoreScreen").SetActive(true);
            }
        }
    }

    public void PopulateHighScores()
    {
        GameObject[] scores = GameObject.FindGameObjectsWithTag("HighScore");

        for (int i = 0; i < 10; i++)
        {
            if (PlayerPrefs.HasKey(i + "HScore"))
            {
                scores[i].GetComponent<Text>().text = PlayerPrefs.GetInt(i + "HScore").ToString("0000");
            }
            else
            {
                scores[i].GetComponent<Text>().text = "0000";
            }

        }
    }

    public void ShowLeaderboard()
    {
#if UNITY_ANDROID
        PlayGamesScript.ShowLeaderboardsUI();
#endif
    }

    // Update is called once per frame
    void Update()
    {
        if(SceneManager.GetSceneAt(1).buildIndex == 2)
        {
            //score += Time.timeScale == 1 ? 1 : 0;

            scoreText.text = "Score - " + score.ToString("0000");

            finalScoreText.text = "Score: " + score.ToString("0000");
        }
    }

    public void AddScore()
    {
        int newScore = 0;
        int oldScore = 0;

        newScore = score;
        for (int i = 0; i < 10; i++)
        {
            if (PlayerPrefs.HasKey(i + "HScore"))
            {
                if(PlayerPrefs.GetInt(i + "HScore") == newScore)
                {
                    break;
                }
                else if (PlayerPrefs.GetInt(i + "HScore") < newScore)
                {
                    // new score is higher than the stored score
                    oldScore = PlayerPrefs.GetInt(i + "HScore");
                    PlayerPrefs.SetInt(i + "HScore", newScore);

                    if (i == 0)
                    {
#if UNITY_ANDROID
                        PlayGamesScript.AddScoreToLeaderboard(newScore);
#endif
                    }

                    newScore = oldScore;
                }
            }
            else
            {
                PlayerPrefs.SetInt(i + "HScore", newScore);

                if (i == 0)
                {
#if UNITY_ANDROID
                    PlayGamesScript.AddScoreToLeaderboard(newScore);
#endif
                }

                newScore = 0;

                break;
            }
        }
    }
}
