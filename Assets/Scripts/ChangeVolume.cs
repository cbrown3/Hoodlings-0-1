﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeVolume : MonoBehaviour
{
    public Slider slider;
    public void UpdateVolume()
    {
        AudioListener.volume = slider.value;
    }
}
