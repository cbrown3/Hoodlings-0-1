﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyString : MonoBehaviour
{
    public Text stringColorText;
    public Text stringCostText;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetString()
    {
        int spaceIndex = stringColorText.text.IndexOf(" ");
        string stringColor = stringColorText.text.Substring(0, spaceIndex);
        print(stringColor);

        spaceIndex = stringCostText.text.IndexOf(" ");

        int stringCost = int.Parse(stringCostText.text.Substring(0, spaceIndex));
        print(stringCost);

        PlayerPrefs.SetInt("Coins", CurrencyManager.totalCoins - stringCost);

        PlayerPrefs.SetInt(stringColor + "String", 1);
    }

    public void ClosePopup()
    {
        Destroy(gameObject);
    }
}
